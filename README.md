# README #

* this is collection of resources on hacking , programming , computation and maths 

# MAKE A DONATION TO APPRICIATE #
### Bitcoin Adress : 1BT9B9sshiXe2FFXGnNMv2sTC4HbbZYww ###

### What is this repository for? ###

* this repository is for people who want to learn computers 
* version 1.0

### Branches covered till now 

* pentest
* advanced pentest
* python

### How do I get set up? ###

* the best PDF reader client for PC is Sumatra pdf : https://www.sumatrapdfreader.org/
* the best ebook manager for PC is Calibre : https://calibre-ebook.com/
* the best media player for PC is Media player classic : https://mpc-hc.org/
* the best note take app with online backup and password protection is laverna for pc (electron based app) : https://laverna.cc/
* Another note taking app solely made to make programming notes because Programming notes don't have to be boring : https://medleytext.net/
* Other main stream tools such as IDE and dev softwares , u would find about them in tools 
* you would need a workplan , so first choose what u want to learn (a lot to choose from 100s of programming languages,
  pentesting, computational mathematics, animation, motion graphics, game dev, web dev, machine learning
  and it goes on since computers are all about infinities) and follow the complete plan form basic to extreme.
* never give up , if u feel frustrated, talk in your community, hit up some EDM. NEVER QUITE, NO TIME TO SLEEP !


### Contribution guidelines ###

* Add new resource links for cources, books and tools but only use non-monetising filehosts like mega.nz,
  google drive and mediafire dont use shitty affiliate filehost likes or it will be dicarded 
* always scan the downloaded resource for malwares and If u find any infected file please commit a change
* if u have been benefited by our efforts and want us to keep up the good work and keep the courses updated and keep adding new courses, then
  please concider donating some BTC at this address   1BT9B9sshiXe2FFXGnNMv2sTC4HbbZYww

### Who do I talk to? ###

* Repo owner or admin
* join our ever growing community of computer scientists
  
  slack    : https://join.slack.com/t/00hack00/shared_invite/MjIzOTAxMDkwNDIzLTE1MDIwMTc1MjEtZTBjODc3YjQ0Yg

  hipchat  : https://00hack00.hipchat.com/invite/733168/979f9e6dcd3ed15e1afbd88f29f4924f?utm_campaign=company_room_link
  
  telegram : https://t.me/joinchat/E0h73g5TBmKz8UZj8Q8BOA
  
  discord  : https://discord.gg/uuuDyWb